package com.java.constants;

public class Constants {

    public static final String ADD_INFO = "========== INSERT INFO ==========";
    public static final String EDIT_INFO = "========== EDIT INFO ==========";
    public static final String DELETE_INFO = "========== DELETE ==========";
    public static final String NEW_INFO = "========== NEW INFORMATION OF STAFF MEMBER ==========";
    public static final String ENTER_NEW_HOURS_WORKED = "=> Enter New Hours Worked : ";
    public static final String ENTER_NEW_RATE = "=> Enter New Rate : ";
    public static final String ENTER_NEW_SALARY = "=> Enter New Salary : ";
    public static final String ENTER_NEW_BONUS = "=> Enter New Bonus ";
    public static final String ENTER_EPLOYEE_ID_TO_UPDATE = "=> Enter Employee ID to Update : ";

    public static final String STAFF_MEMBER_NOT_EXIST(int staffID) {
        return "There is no Staff Member ID : " + staffID + "...";
    }
    public static final String CHOOSE_OPTION           = "=> Choose option(1-4) : ";

    public static final String ENTER_STAFF_ID = "=> Enter Staff Member's ID    : ";
    public static final String ENTER_STAFF_NAME = "=> Enter Staff Member's Name    : ";
    public static final String ENTER_STAFF_ADDRESS = "=> Enter Staff Member's Address    : ";
    public static final String ENTER_RATE = "=> Enter Rate : ";
    public static final String ENTER_SALARY = "=> Enter Staff's Salary : ";
    public static final String ENTER_BONUS = "=> Enter Staff's Bonus ";
    public static final String ENTER_HOURS_WORKED = "=> Enter Hours Worked : ";

    public static final String NO_STAFF_MEMBER = "       No Staff Member Here";

    public static final String ENTER_EPLOYEE_ID_TO_REMOVE = "=> Enter Employee ID to Remove : ";

    public static final String REMOVE_SUCCESSFULLY         = "\nRemoved Successfully";

    public static final String GOODBYE                 = "\uD83E\uDD17 Good Bye! \uD83E\uDD17";
    public static final String ALERT_INPUT_NUMBER_1_TO_4 = "Please input number 1 to 4";

}
