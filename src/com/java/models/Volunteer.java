package com.java.models;

public class Volunteer extends StaffMember {

    public Volunteer(int id, String name, String address) {
        super(id, name, address);
    }

    @Override
    public Double pay() {
        return 0.0;
    }

    @Override
    public String toString() {
        return super.toString() + "Thank!";
    }

    public static void main(String[] args) {
        Volunteer volunteer = new Volunteer(1, "", "");
        System.out.println(volunteer.hashCode());
    }
}
