package com.java.models;

public abstract class StaffMember implements Comparable<StaffMember> {
    private int id;
    protected String name;
    protected String address;

    public StaffMember(int id, String name, String addres) {
        this.id = id;
        this.name = name;
        this.address = addres;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public abstract Double pay();

    @Override
    public int compareTo(StaffMember o) {
        return this.name.compareTo(o.name);
    }

    @Override
    public String toString() {
        return "\nID : " + id +" \nName : " + name + "\nAddress : " + address + "\n";
    }
}
