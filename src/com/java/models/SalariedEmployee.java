package com.java.models;

public class SalariedEmployee extends StaffMember {
    private Double salary;
    private Double bonus;

    public SalariedEmployee(int id, String name, String address, Double salary, Double bonus) {
        super(id, name, address);
        setSalary(salary);
        setBonus(bonus);
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        if (salary != null) {
            this.salary = salary;
        } else {
            this.salary = null;
        }

    }

    public Double getBonus() {
        return bonus;
    }

    public void setBonus(Double bonus) {
        if (bonus != null) {
            this.bonus = bonus;
        } else {
            this.bonus = null;
        }
    }

    public Double pay() {
        if (this.salary == null) {
            return this.bonus;
        } else if (this.bonus == null) {
            return this.salary;
        } else if (this.salary == null && this.bonus == null) {
            return null;
        } else {
            if (this.salary < 0.0 && this.salary < 0.0) {
                return null;
            }
        }
        return salary + bonus;
    }

    @Override
    public String toString() {
        return super.toString() + "Salary : " + getSalary() + "\nBonus : " + getBonus() + "\nPayment : " + pay() + "";
    }
}
