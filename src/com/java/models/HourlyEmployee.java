package com.java.models;

public class HourlyEmployee extends StaffMember {
    private Integer hoursWorked;
    private Double rate;

    public HourlyEmployee(int id, String name, String address, Double rate, Integer hoursWorked) {
        super(id, name, address);
        setHoursWorked(hoursWorked);
        setRate(rate);
    }

    public Integer getHoursWorked() {
        return hoursWorked;
    }

    public void setHoursWorked(Integer hoursWorked) {
        if (hoursWorked != null) {
            this.hoursWorked = hoursWorked;
        } else {
            this.hoursWorked = null;
        }
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        if (rate != null) {
            this.rate = rate;
        } else {
            this.rate = null;
        }
    }

    public Double pay() {
        if (this.rate == null || this.hoursWorked == null) {
            return null;
        } else {
            if (this.rate < 0.0 && this.hoursWorked < 0.0) {
                return null;
            }
        }
        return hoursWorked * rate;
    }

    @Override
    public String toString() {
        return super.toString() + "Hours Worked : " + getHoursWorked() + "\nRate : " + getRate() + "\nPayment : " + pay() + "";
    }
}
