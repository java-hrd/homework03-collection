package com.java.views;

import com.java.constants.Constants;
import com.java.models.*;
import com.java.utils.UtilKit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EmployeeView {
    List<StaffMember> objects;
    public EmployeeView() {
        objects = new ArrayList<>();
        initEmployee();
    }

    public void menu() {
        line();
        System.out.print("1). Add Employee \t");
        System.out.print("2). Edit \t");
        System.out.print("3). Remove \t");
        System.out.print("4). Exit \n\n");

        chooseMenu();
    }


    // Menu
    private void chooseMenu() {
        int chooseNumber = (int) UtilKit.inputNumber(Constants.CHOOSE_OPTION);
        switch (chooseNumber) {
            case 1:
                addEmployee();
                showEmployee();
                break;
            case 2:
                editEmployee();
                showEmployee();
                break;
            case 3:
                removeEmployee();
                showEmployee();
                break;
            case 4:
                System.out.println(Constants.GOODBYE);
                System.exit(0);
                break;
            default:
                System.out.println(Constants.ALERT_INPUT_NUMBER_1_TO_4);
                showEmployee();
                break;

        }
    }

    // Initialize Staff
    private void initEmployee() {
        StaffMember staffMember1 = new Volunteer(1, "Sam", "123 Main Love");
        StaffMember employee1 = new HourlyEmployee(2, "Diane", "678 Fifth Ave", 90.56, 20);
        StaffMember employee2 = new SalariedEmployee(3, "Carla", "456 Off Line", 1246.15, 345.90);

        objects.add(staffMember1);
        objects.add(employee1);
        objects.add(employee2);

        Collections.sort(objects);
    }

    // Add Employee
    private void addEmployee() {
        line();
        System.out.print("1). Volunteer \t");
        System.out.print("2). Hourly Emp \t");
        System.out.print("3). Salaried Emp \t");
        System.out.print("4). Back \n\n");

        chooseStaff();

    }

    // Menu choosing kind of staff
    private void chooseStaff() {
        int chooseNumber = (int) UtilKit.inputNumber(Constants.CHOOSE_OPTION);
        switch (chooseNumber) {
            case 1:
                addVolunteer();
                break;
            case 2:
                addHourlyEmp();
                break;
            case 3:
                addSalariedEmp();
                showEmployee();
                break;
            case 4:
                showEmployee();
                break;
            default:
                System.out.println(Constants.ALERT_INPUT_NUMBER_1_TO_4);
                showEmployee();
                break;

        }
    }

    // add salaried employee
    private void addSalariedEmp() {
        int id;
        String name;
        String address;
        double salary;
        double bonus;

        System.out.println(Constants.ADD_INFO);

        id = (int) UtilKit.inputNumber(Constants.ENTER_STAFF_ID);

        do {
            name = UtilKit.inputString(Constants.ENTER_STAFF_NAME);
        }while(!UtilKit.containsSpece(name));

        do {
            address = UtilKit.inputString(Constants.ENTER_STAFF_ADDRESS);
        }while(!UtilKit.containsSpece(address));

        salary = UtilKit.inputNumber(Constants.ENTER_SALARY);
        bonus = UtilKit.inputNumber(Constants.ENTER_BONUS);

        objects.add(new SalariedEmployee(id, name, address, salary, bonus));
        Collections.sort(objects);
        showEmployee();
    }

    // add volunteer
    private void addVolunteer() {
        int id;
        String name;
        String address;

        System.out.println(Constants.ADD_INFO);
        id = (int) UtilKit.inputNumber(Constants.ENTER_STAFF_ID);

        do {
            name = UtilKit.inputString(Constants.ENTER_STAFF_NAME);
        }while(!UtilKit.containsSpece(name));

        do {
            address = UtilKit.inputString(Constants.ENTER_STAFF_ADDRESS);
        }while(!UtilKit.containsSpece(address));

        objects.add(new Volunteer(id, name, address));
        Collections.sort(objects);
        showEmployee();
    }

    // add hourly employee
    private void addHourlyEmp() {
        int id;
        String name;
        String address;
        int hoursWorked;
        double rate;

        System.out.println(Constants.ADD_INFO);

        id = (int) UtilKit.inputNumber(Constants.ENTER_STAFF_ID);

        do {
            name = UtilKit.inputString(Constants.ENTER_STAFF_NAME);
        }while(!UtilKit.containsSpece(name));

        do {
            address = UtilKit.inputString(Constants.ENTER_STAFF_ADDRESS);
        }while(!UtilKit.containsSpece(address));

        hoursWorked = (int) UtilKit.inputNumber(Constants.ENTER_HOURS_WORKED);
        rate = UtilKit.inputNumber(Constants.ENTER_RATE);

        objects.add(new HourlyEmployee(id, name, address, rate, hoursWorked));
        Collections.sort(objects);
        showEmployee();
    }

    // show employee
    public void showEmployee() {
        for (StaffMember staffMember : objects) {
            System.out.println(staffMember.toString());
            line();
        }

        menu();
    }

    // edit staff
    private void editEmployee() {
        boolean status = false;
        String tmpName;
        int tmpHoursWorked;
        double tmpRate;
        double tmpSalary;
        double tmpBonus;

        System.out.println(Constants.EDIT_INFO);
        if (objects != null && objects.size() != 0) {
            int tmpStaffId = (int) UtilKit.inputNumber(Constants.ENTER_EPLOYEE_ID_TO_UPDATE);
            for (int i=0; i<objects.size(); i++) {
                if (objects.get(i).getId() == tmpStaffId) {
                    System.out.println(objects.get(i).toString());
                    System.out.println(Constants.NEW_INFO);
                    do {
                        tmpName = UtilKit.inputString(Constants.ENTER_STAFF_NAME);
                    }while(!UtilKit.containsSpece(tmpName));

                    // if staff is volunteer
                    if (objects.get(i) instanceof Volunteer) {

                    }

                    // if staff is hourly employee
                    else if (objects.get(i) instanceof HourlyEmployee) {
                        tmpHoursWorked = (int) UtilKit.inputNumber(Constants.ENTER_NEW_HOURS_WORKED);
                        tmpRate = UtilKit.inputNumber(Constants.ENTER_NEW_RATE);

                        ((HourlyEmployee) objects.get(i)).setHoursWorked(tmpHoursWorked);
                        ((HourlyEmployee) objects.get(i)).setRate(tmpRate);
                    }

                    // if staff is salaried employee
                    else if (objects.get(i) instanceof SalariedEmployee) {
                        tmpSalary = UtilKit.inputNumber(Constants.ENTER_NEW_SALARY);
                        tmpBonus = UtilKit.inputNumber(Constants.ENTER_NEW_BONUS);

                        ((SalariedEmployee) objects.get(i)).setSalary(tmpSalary);
                        ((SalariedEmployee) objects.get(i)).setBonus(tmpBonus);
                    }

                    objects.get(i).setName(tmpName);
                    objects.set(i, objects.get(i));
                    status = true;
                    break;
                } else {
                    status = false;
                }
            }
            if (status) {

            } else {
                System.out.println(Constants.STAFF_MEMBER_NOT_EXIST(tmpStaffId));
            }
        } else {
            System.out.println(Constants.NO_STAFF_MEMBER);
        }

    }

    // remove employee
    private void removeEmployee() {
        boolean status = false;
        System.out.println(Constants.DELETE_INFO);
        if (objects != null && objects.size() != 0) {
            int tmpStaffId = (int) UtilKit.inputNumber(Constants.ENTER_EPLOYEE_ID_TO_REMOVE);
            for (StaffMember sm : objects) {
                if (sm.getId() == tmpStaffId) {
                    objects.remove(sm);
                    System.out.println(sm.toString());
                    System.out.println(Constants.REMOVE_SUCCESSFULLY);
                    status = true;
                    break;
                } else {
                    status = false;
                }
            }
            if (!status) {
                System.out.println(Constants.STAFF_MEMBER_NOT_EXIST(tmpStaffId));
            }
        } else {
            System.out.println(Constants.NO_STAFF_MEMBER);
        }
    }

    static void line() {
        System.out.println("-----------------------------------");
    }
}
